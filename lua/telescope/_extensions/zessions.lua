local Path = require("plenary.path")
local actions = require("telescope.actions")
local get_dropdown = require("telescope.themes").get_dropdown


local Config = {}

function Config:new()
  local obj = {
    cwd = Path:new(vim.fn.stdpath("data"), "sessions"):expand(),
    ask = true,
  }

  setmetatable(obj, self)
  self.__index = self
  return obj
end

function Config:reset()
  self.cwd = Path:new(vim.fn.stdpath("data"), "sessions"):expand()
  self.ask = true
end

function Config:update(opts)
  opts     = opts or {}
  self.cwd = opts.cwd or self.cwd
  self.ask = opts.cwd or self.cwd
  return self
end

local config = Config:new()


local get_session_file = function(session_name)
  if not session_name or session_name == "" then
    error("A session name must be given !")
  end
  return Path:new(config.cwd, session_name)
end

local confirmed = function(prompt)
  if config.ask == false then
    return true
  end
  local answer = vim.fn.input(string.format("%s [y/N]", prompt))
  print(" ")
  return answer:lower() == "y"
end

local check_session_for = function(operation, session_name)
  local session_file = get_session_file(session_name)
  local file_exists  = session_file:is_file()

  if operation == "save" then
    local ok = not file_exists or confirmed(string.format("Overwrite '%s' ?", session_name))
    return ok, session_file
  end

  if operation == "delete" then
    local ok = not file_exists or confirmed(string.format("Delete '%s' ?", session_name))
    return ok, session_file
  end

  if operation == "restore" then
    return file_exists, session_file
  end

end


local save_session = function(session_name)
  local ok, session_file = check_session_for("save", session_name)
  if not ok then return end
  print(string.format("Saving new session '%s' (%s)", session_name, session_file))
  vim.cmd("mksession! "..session_file:absolute())
end

local delete_session = function(session_name)
  local ok, session_file = check_session_for("delete", session_name)
  if not ok then return end
  print(string.format("Deleting session '%s' (%s)", session_name, session_file))
  session_file:rm()
end

local restore_session = function(session_name)
  local ok, session_file = check_session_for("restore", session_name)
  if not ok then return end
  print(string.format("Restoring session '%s' (%s)", session_name, session_file))
  vim.cmd("source "..session_file:absolute())
end


local zactions = {}

zactions.save = function(prompt_bufnr)
  local selection = actions.get_current_line()
  actions.close(prompt_bufnr)
  save_session(selection)
end

zactions.delete = function(prompt_bufnr)
  local selection = actions.get_selected_entry()
  actions.close(prompt_bufnr)
  delete_session(selection.value)
end

zactions.restore = function(prompt_bufnr)
  local selection = actions.get_selected_entry()
  actions.close(prompt_bufnr)
  restore_session(selection.value)
end


local sessions = function(opts)
  opts = opts or {}

  local default_opts = {
    cwd = config:update(opts).cwd,
    attach_mappings = function(prompt_bufnr, map)
      map("i", "<C-s>", zactions.save)
      map("i", "<C-d>", zactions.delete)
      actions.select_default:replace(zactions.restore)
      return true
    end
  }
  local dropdown_opts = get_dropdown({
      prompt_title = "Sessions",
      previewer = false,
      width = 30,
  }, opts)

  opts = vim.tbl_deep_extend("force", default_opts, dropdown_opts, opts)
  require("telescope.builtin").find_files(opts)
end

return require("telescope").register_extension {
  exports = {
    reset    = function() config:reset() end,
    setup    = function(otps) config:update(opts) end,
    sessions = sessions,
  }
}
